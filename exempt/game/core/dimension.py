import arcade 
class Dimension:
    """
    This class holds all the information about the
    different Dimension(s).
    """

    def __init__(self):
        #This holds hte background image
        self.background = None

        self.dimensiontype = None

        #This holds the list of structures
        self.wall_list = None

        #This holds the list of enemies
        self.enemy_list = None

        #This holds hte player information
        self.player = None

        #This holds the list of available items
        self.item_list = None

    def setup_walls():
        # -- Set up the walls
        # Create bottom and top row of boxes
        # This y loops a list of two, the coordinate 0, and just under the top of window
        for y in (0, SCREEN_HEIGHT - SPRITE_SIZE):
            # Loop for each box going across
            for x in range(0, SCREEN_WIDTH, SPRITE_SIZE):
                #todo - fix this hardcoded value later
                wall = arcade.Sprite(":resources:images/tiles/boxCrate_double.png", .25)
                wall.left = x
                wall.bottom = y
                self.wall_list.append(wall)